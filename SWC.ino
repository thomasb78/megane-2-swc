#include "Arduino.h"
#include "MCP4131.h"
#include "io.h"

/*
The setup function is called once at startup of the sketch
                  +-\/-+
      (RES_)PC6  1|    |28  PC5 (AI 5) (D 19)
      (D 0) PD0  2|    |27  PC4 (AI 4) (D 18)
      (D 1) PD1  3|    |26  PC3 (AI 3) (D 17)
  INT0(D 2) PD2  4|    |25  PC2 (AI 2) (D 16)
  INT1(D 3) PD3  5|    |24  PC1 (AI 1) (D 15)
      (D 4) PD4  6|    |23  PC0 (AI 0) (D 14)
            VCC  7|    |22  GND
            GND  8|    |21  AREF
            PB6  9|    |20  AVCC
            PB7 10|    |19  PB5 (D 13)    | SCK  -+
 PWM+ (D 5) PD5 11|    |18  PB4 (D 12)    | MISO  |=> SPI
 PWM+ (D 6) PD6 12|    |17  PB3 (D 11) PWM| MOSI -+
      (D 7) PD7 13|    |16  PB2 (D 10) PWM
      (D 8) PB0 14|    |15  PB1 (D 9)  PWM
                  +----+

	    C P|
	 	O I|
		L N|
		   +-----OUT-----
		UC | PD6|PD7|PB0
		ROW| 0  |1  |2  |
		PIN|   2|  4|  6|
	UC   --+-------------
  | PD1 0 1| R1  PAU  V+
IN| PD2 1 3| R3  UP1 DOWN
  | PD3 2 5| R2  UP2  V-

	WIRES COLORS:
	* ORANGE -- 1
	* PINK   -- 2
	* WHITE  -- 3
	* VIOLET -- 4
	* GREEN  -- 5
	* GREY   -- 6

*/

unsigned long debounce_time[3][3];
unsigned long rsw_sending_time = 0L;

char reading[3][3];
char last_reading[3][3];
char last_rsw_pos = -1;

#define PIN_OUT0 6 // PD6
#define PIN_OUT1 7 // PD7
#define PIN_OUT2 8 // PB0

#define PIN_IN0 1 // PD1
#define PIN_IN1 2 // PD2
#define PIN_IN2 3 // PD3

DigitalPin out0(PIN_OUT0);
DigitalPin out1(PIN_OUT1);
DigitalPin out2(PIN_OUT2);
DigitalPin in0(PIN_IN0);
DigitalPin in1(PIN_IN1);
DigitalPin in2(PIN_IN2);

DigitalPin led1(9);
DigitalPin led2(10);

#define SLAVE_SPI_PIN 0 // PD0

#define DEBOUNCE_DELAY 100

#define RSW1 reading[0][0]
#define RSW2 reading[0][2]
#define RSW3 reading[0][1]

#define PAUSE reading[1][0]
#define UP1 reading[1][1]
#define UP2 reading[1][2]

#define VOLPLUS reading[2][0]
#define DOWN1 reading[2][1]
#define VOLMINUS reading[2][2]

/*
 * Resistor configuration (max 128)
 */
#define RSW_UP_COMMAND_VALUE 0
#define RSW_DOWN_COMMAND_VALUE 5
#define PAUSE_COMMAND_VALUE 10
#define VOLPLUS_COMMAND_VALUE 20
#define VOLMINUS_COMMAND_VALUE 35
#define UP1_COMMAND_VALUE 50
#define UP2_COMMAND_VALUE 80
#define VOLCOMBO_COMMAND_VALUE 105
#define DOWN_COMMAND_VALUE 128

// DEBUG
//#define NO_DEBOUNCE
//#define KEYBOARD_DEBUG

MCP4131 controller(SLAVE_SPI_PIN);

void
setup()
{
	controller.activateR0Terminals(false);

	// Clean debounce data
	memset((void*)debounce_time, 0, sizeof(debounce_time));
	memset((void*)reading, 0, sizeof(reading));
	memset((void*)last_reading, 0, sizeof(last_reading));
	out0.set_mode(DigitalPin::MODE_OUTPUT);
	out1.set_mode(DigitalPin::MODE_OUTPUT);
	out2.set_mode(DigitalPin::MODE_OUTPUT);

	in0.set_mode(DigitalPin::MODE_INPUT);
	in1.set_mode(DigitalPin::MODE_INPUT);
	in2.set_mode(DigitalPin::MODE_INPUT);

	led1.set_mode(DigitalPin::MODE_OUTPUT);
	led2.set_mode(DigitalPin::MODE_OUTPUT);

	led1.set_fast(false);
	led2.set_fast(false);

	out0.set_fast(false);
	out1.set_fast(false);
	out2.set_fast(false);
}

void
debounce_pin(const int row, const int col,  const char value)
{
	char val = value ? 1 : 0;
#ifdef NO_DEBOUNCE
	reading[row][col] = val;
	return;
#else
	if (last_reading[row][col] != val){
		debounce_time[row][col] = millis();
	} else if ( (millis() - debounce_time[row][col]) > DEBOUNCE_DELAY ){
		reading[row][col] = val;
	}
	last_reading[row][col] = val;
#endif
}

void
send_command(int command)
{
	controller.writeWiper(command);
	controller.activateR0Terminals(true);
}

void
scan_row(const uint8_t row)
{
	out0.set_fast(row == 0);
	out1.set_fast(row == 1);
	out2.set_fast(row == 2);

	debounce_pin(row, 0, in0.get());
	debounce_pin(row, 1, in1.get());
	debounce_pin(row, 2, in2.get());

	out0.set_fast(false);
	out1.set_fast(false);
	out2.set_fast(false);
}
#ifdef KEYBOARD_DEBUG
void display(int row, int col)
{
	for (int i = 0; i < row + 1; ++i){
		led1.set_fast(true);
		delay(500);
		led1.set_fast(false);
		delay(500);
	}
--
	for (int i = 0; i < col + 1; ++i){
		led2.set_fast(true);
		delay(500);
		led2.set_fast(false);
		delay(500);
	}

	delay(500);
}
#endif
void
loop()
{
	if( rsw_sending_time != 0 && ((millis() - rsw_sending_time) < 200)){
		/*
		 * Give the rotary switch command a delay to perform (no key holding in this case)
		 */
		return;
	}
	rsw_sending_time = 0L;

	scan_row(0);
	scan_row(1);
	scan_row(2);

#ifdef KEYBOARD_DEBUG
	for(int i = 0; i < 3;++i){
		for(int j = 0; j < 3;++j){
			if (reading[i][j]){
				display(i, j);
			}
		}
	}
	return;
#endif

	/* Make sure rotary switch is in correct position (1 unique junction)*/
	bool rsw_valid_pos = (RSW1 + RSW2 + RSW3) == 1;

	if (rsw_valid_pos){
		int code = -1;
		if (RSW1) code = 0;
		else if (RSW2) code = 1;
		else if (RSW3) code = 2;

		if (code != last_rsw_pos){
			bool up = (last_rsw_pos == 0 && code == 1) ||
					  (last_rsw_pos == 1 && code == 2) ||
					  (last_rsw_pos == 2 && code == 0);

			bool down = (last_rsw_pos == 2 && code == 1) ||
					  (last_rsw_pos == 1 && code == 0) ||
					  (last_rsw_pos == 0 && code == 2);

			if (up){
				send_command(RSW_UP_COMMAND_VALUE);
				led1.set_fast(true);
			}

			if (down){
				send_command(RSW_DOWN_COMMAND_VALUE);
				led2.set_fast(true);
			}
			last_rsw_pos = code;
			if (up || down){
				rsw_sending_time = millis();
				return;
			}
		}
	}

	if (VOLPLUS && VOLMINUS){
		send_command(VOLCOMBO_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}

	if (VOLPLUS){
		send_command(VOLPLUS_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}
	if (VOLMINUS){
		send_command(VOLMINUS_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}
	if (PAUSE){
		send_command(PAUSE_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}

	if (UP1){
		send_command(UP1_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}

	if (UP2){
		send_command(UP2_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}

	if (DOWN1){
		send_command(DOWN_COMMAND_VALUE);
		led1.set_fast(true);
		return;
	}

	led1.set_fast(false);
	led2.set_fast(false);

	controller.activateR0Terminals(false);
}
