#ifndef MCP4131_H
#define MCP4131_H

#include "Arduino.h"
#include <SPI.h>

class MCP4131
{
    public:
        MCP4131(int slave_select_pin);
        byte readWiper();
        void writeWiper(unsigned int wiperValue);
        void activateR0Terminals(bool a);
    private:
        void enableChip(bool);
        byte sendCommand(byte address, char command, unsigned int data);
        int slaveSelectPin;
        SPISettings spiSettings;
};

#endif

