#include "MCP4131.h"

const static byte ADDRESS_WIPER0 = 0x0;
const static byte ADDRESS_TCON = 0x4;

const static unsigned char COMMAND_WRITE = B00;
const static unsigned char COMMAND_READ = B11;

MCP4131::MCP4131(int slavePin) :
		spiSettings(SPISettings(250000, MSBFIRST, SPI_MODE0))
{
    slaveSelectPin = slavePin;
	pinMode(slavePin, OUTPUT);
    SPI.begin();
}

byte
MCP4131::readWiper()
{
    return sendCommand(ADDRESS_WIPER0, COMMAND_READ, 255);
}

void
MCP4131::writeWiper(unsigned int wiperValue)
{
    sendCommand(ADDRESS_WIPER0, COMMAND_WRITE, wiperValue);
}


byte
MCP4131::sendCommand(byte address, char command, unsigned int data)
{
    byte transferByte = (address << 4) | (command << 2);

	SPI.beginTransaction(spiSettings);
    enableChip(true);
    SPI.transfer(transferByte);
    byte result = SPI.transfer(data);
    enableChip(false);
    SPI.endTransaction();

    return result;
}

void
MCP4131::activateR0Terminals(bool a)
{
	if (a){
		sendCommand(ADDRESS_TCON, COMMAND_WRITE, 0b000001111);
	} else {
		sendCommand(ADDRESS_TCON, COMMAND_WRITE, 0b000001000);
	}
}

void
MCP4131::enableChip(bool e)
{
    digitalWrite(slaveSelectPin, e == true ? LOW : HIGH);
}

